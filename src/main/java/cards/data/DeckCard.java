package cards.data;

import java.util.Objects;

/**
 * i wanted to represent cards in this way for integration and
 * extendability reasons.
 * 
 * for instance: something is going to be consuming this api (well,
 * not really, but lets just pretend). it would be much easier for
 * a frontend to integrate with an object like { suit: "SPADES", face: "_2" }
 * verses have to parse a string "2-spades". doing it this way also lets
 * us control the ordering in a more natural way.
 * 
 * also, I don't say extendability lightly. a general rule that i
 * keep in mind while designing software is minimal future proofing. but
 * i thought this was an obvious one. for instance, we should be able to 
 * add different suits and make different values, add colors maybe. also,
 * if we wanted to "real" card app, there most likely would be different
 * types of decks because decks change for locales.
 */
public class DeckCard implements Comparable<DeckCard> {
  
  public enum Suit {
    SPADES(1),
    DIAMONDS(2),
    CLUBS(3),
    HEARTS(4);
    
    private final int order;

    Suit(int order) {
      this.order = order;
    }
    
    public int order() {
      return order;
    }
  }
  
  public enum Face {
    ACE(1),
    _2(2),
    _3(3),
    _4(4),
    _5(5),
    _6(6),
    _7(7),
    _8(8),
    _9(9),
    _10(10),
    JACK(11),
    QUEEN(12),
    KING(13);

    private final int order;

    Face(int order) {
      this.order = order;
    }

    public int order() {
      return order;
    }
  }
  
  public final Suit suit;
  public final Face face;

  public DeckCard(Suit suit, Face face) {
    this.suit = Objects.requireNonNull(suit);
    this.face = Objects.requireNonNull(face);
  }

  @Override
  public int compareTo(DeckCard that) {
    return this.suit == that.suit 
        ? this.suit.order() - that.suit.order()
        : this.face.order() - that.face.order();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) { return true; }
    if (!(o instanceof DeckCard)) { return false; }

    DeckCard deckCard = (DeckCard) o;

    if (suit != deckCard.suit) { return false; }
    return face == deckCard.face;

  }

  @Override
  public int hashCode() {
    int result = suit.hashCode();
    result = 31 * result + face.hashCode();
    return result;
  }
}
