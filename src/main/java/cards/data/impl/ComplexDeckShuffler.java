package cards.data.impl;

import cards.data.Deck;
import cards.data.DeckCard;
import cards.data.DeckShuffler;
import com.google.common.collect.ImmutableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * this shuffler tries to mimic the actual way humans shuffle cards.
 */
public class ComplexDeckShuffler implements DeckShuffler {
  
  private static final Logger logger = LogManager.getLogger();
  
  @Override
  public Deck shuffle(Deck target) {
    int iterations = (int) (Math.random() * 8) + 2;
    logger.debug("shuffling deck {} {} times", target.name, iterations);
    
    List<DeckCard> cards = new ArrayList<>(target.cards);
    for(int i = 0; i < iterations; i++) {
      cards = shuffleOnce(cards);
    }
    
    // now... lets cut the deck for fun
    List<DeckCard> result = cut(cards);
    
    return new Deck(target.name, ImmutableList.copyOf(result));
  }
  
  private List<DeckCard> shuffleOnce(List<DeckCard> cards) {
    // array lists are light objects, so making a bunch of these 
    // 'shouldnt' make for bad performance.
    List<DeckCard> result = new ArrayList<>();
    int split = split(cards);
    
    // sub-lists cannot be concurrently modified so we have to copy
    List<DeckCard> left = new ArrayList<>(cards.subList(0, split));
    List<DeckCard> right = new ArrayList<>(cards.subList(split, cards.size()));
    
    while(!left.isEmpty() && !right.isEmpty()) {
      // lets mix up which side cards get pulled from first as well
      if (Math.random() > 0.5D) {
        pull(left, result);
        pull(right, result);
      }
      else {
        pull(right, result);
        pull(left, result);
      }
    }

    if (!left.isEmpty()) result.addAll(left);
    if (!right.isEmpty()) result.addAll(right);
    
    return result;
  }

  private List<DeckCard> cut(List<DeckCard> cards) {
    int split = split(cards);
    List<DeckCard> left = cards.subList(0, split);
    List<DeckCard> right = cards.subList(split, cards.size());
    List<DeckCard> result = new ArrayList<>();
    result.addAll(right);
    result.addAll(left);
    return result;
  }
  
  private int split(List<DeckCard> cards) {
    int size = (cards.size() / 2);
    // when you split a deck, its not always perfect. so lets move the center a little.
    size += (int) ((Math.random() - 0.5D) * 6);
    return size;
  }
  
  private void pull(List<DeckCard> from, List<DeckCard> to) {
    // pull size of at least one 
    int pullSize = (int) (Math.round(Math.random() * 2) + 1);
    for(int i = 0; i < pullSize && !from.isEmpty(); i++) {
      to.add(from.remove(0));
    }
  }
  
}
