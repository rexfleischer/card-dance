package cards.data.impl;

import cards.data.Deck;
import cards.data.DeckDAO;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * NOTE: because decks are immutable. we dont have to worry about
 *       returning new instances of decks. 
 */
public class DeckDAOImpl implements DeckDAO {
  
  private final Map<String, Deck> decks = new ConcurrentHashMap<>();
  
  @Override
  public Set<String> getDeckNames() {
    return new LinkedHashSet<>(decks.keySet());
  }

  @Override
  public Deck getDeck(String name) {
    return decks.get(name);
  }

  @Override
  public void saveDeck(Deck deck) {
    decks.put(deck.name, deck);
  }

  @Override
  public boolean deleteDeck(String name) {
    return decks.remove(name) != null;
  }
  
}
