package cards.data.impl;

import cards.data.Deck;
import cards.data.DeckCard;
import cards.data.DeckShuffler;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * simple shuffler that just uses the default Collections.shuffle algorithm
 */
public class SimpleDeckShuffler implements DeckShuffler {
  @Override
  public Deck shuffle(Deck target) {
    List<DeckCard> cards = new ArrayList<>(target.cards);
    Collections.shuffle(cards);
    return new Deck(target.name, ImmutableList.copyOf(cards));
  }
}
