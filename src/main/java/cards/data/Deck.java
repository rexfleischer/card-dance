package cards.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class Deck {

  /**
   * this is the initial deck in an common initial ordering as if 
   * someone just opened the deck of cards.
   */
  public static final ImmutableList<DeckCard> INITIAL_CARDS;
  static {
    INITIAL_CARDS = Arrays
        .stream(DeckCard.Suit.values())
        .flatMap(suit -> Arrays
            .stream(DeckCard.Face.values())
            .map(face -> new DeckCard(suit, face)))
        .sorted()
        .collect(Collectors.collectingAndThen(Collectors.toList(), ImmutableList::copyOf));
  }
  
  public final String name;
  public final ImmutableList<DeckCard> cards;

  public Deck(String name, ImmutableList<DeckCard> cards) {
    // dont want to loose any cards
    Preconditions.checkArgument(cards.size() == INITIAL_CARDS.size());
    this.name = Objects.requireNonNull(name);
    this.cards = Objects.requireNonNull(cards);
  }

  public Deck(String name) {
    this.name = Objects.requireNonNull(name);
    this.cards = INITIAL_CARDS;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) { return true; }
    if (!(o instanceof Deck)) { return false; }

    Deck deck = (Deck) o;

    if (!name.equals(deck.name)) { return false; }
    return cards.equals(deck.cards);

  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + cards.hashCode();
    return result;
  }
}
