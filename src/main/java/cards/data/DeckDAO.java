package cards.data;

import java.util.Set;

public interface DeckDAO {
  
  Set<String> getDeckNames();
  Deck getDeck(String name);
  void saveDeck(Deck deck);
  boolean deleteDeck(String name);
  
}
