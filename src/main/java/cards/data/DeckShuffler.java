package cards.data;

public interface DeckShuffler {
  
  Deck shuffle(Deck target);
  
}
