package cards;

import cards.data.impl.ComplexDeckShuffler;
import cards.inject.InjectorFactory;
import cards.server.ServerManager;
import com.google.inject.Injector;

public class Main {
  
  public static void main(String... args) throws Exception {
    
    Injector injector = InjectorFactory.get(8080, ComplexDeckShuffler.class);
    
    ServerManager manager = injector.getInstance(ServerManager.class);
    try {
      manager.start();
      manager.join();
    }
    finally {
      manager.stop();
    }
    
  }
  
}
