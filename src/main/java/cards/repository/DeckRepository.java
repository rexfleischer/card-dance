package cards.repository;

import cards.data.Deck;
import cards.data.DeckDAO;
import cards.data.DeckShuffler;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.Set;

/**
 * this class represents the business logic. I think 
 * its important to have the business logic always 
 * contained in an easy to use object like this. it makes for
 * easier testing and better separation of concerns.
 */
@Singleton
public class DeckRepository {
  
  private final DeckDAO decks;
  private final DeckShuffler shuffler;

  @Inject
  public DeckRepository(DeckDAO decks, DeckShuffler shuffler) {
    this.decks = decks;
    this.shuffler = shuffler;
  }

  public Set<String> getDeckNames() {
    return decks.getDeckNames();
  }

  public Deck getDeck(String name) throws DeckNotFoundException {
    return requireDeck(name);
  }

  public Deck ensureDeck(String name) {
    Deck check = decks.getDeck(name);
    if (check == null) {
      check = new Deck(name);
      decks.saveDeck(check);
    }
    return check;
  }

  public void deleteDeck(String name) throws DeckNotFoundException {
    if (!decks.deleteDeck(name)) {
      throw new DeckNotFoundException(name);
    }
  }

  public Deck shuffleDeck(String name) throws DeckNotFoundException {
    Deck target = requireDeck(name);
    Deck result = shuffler.shuffle(target);
    decks.saveDeck(result);
    return result;
  }
  
  private Deck requireDeck(String name) throws DeckNotFoundException {
    Deck check = decks.getDeck(name);
    if (check == null) {
      throw new DeckNotFoundException(name);
    }
    return check;
  }
  
}
