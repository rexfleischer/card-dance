package cards.repository;

public class DeckNotFoundException extends Exception {
  
  public DeckNotFoundException(String name) {
    super(name);
  }
  
  public String getDeckName() {
    return getMessage();
  }
  
}
