package cards.inject;

import cards.data.DeckShuffler;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class InjectorFactory {
  
  public static Injector get(int port, Class<? extends DeckShuffler> shuffler) {
    return Guice.createInjector(
        new ConfigModule(port, shuffler),
        new PersistenceModule(),
        new SerializationModule()
    );
  }
  
}
