package cards.inject;

import com.dampcake.gson.immutable.ImmutableAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class SerializationModule extends AbstractModule {
  
  @Override
  protected void configure() {
    
  }
  
  @Provides
  @Singleton
  public Gson getGson() {
    return new GsonBuilder()
        .registerTypeAdapterFactory(ImmutableAdapterFactory.forGuava())
        .setPrettyPrinting()
        .create();
  }
  
}
