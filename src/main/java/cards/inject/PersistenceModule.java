package cards.inject;

import cards.data.DeckDAO;
import cards.data.impl.DeckDAOImpl;
import com.google.inject.AbstractModule;

public class PersistenceModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(DeckDAO.class).to(DeckDAOImpl.class);
  }
}
