package cards.inject;

import cards.data.DeckShuffler;
import cards.server.ServerPort;
import com.google.inject.AbstractModule;

public class ConfigModule extends AbstractModule {
  
  private final int port;
  private final Class<? extends DeckShuffler> shuffler;

  public ConfigModule(int port, Class<? extends DeckShuffler> shuffler) {
    this.port = port;
    this.shuffler = shuffler;
  }

  @Override
  protected void configure() {
    bindConstant().annotatedWith(ServerPort.class).to(port);
    bind(DeckShuffler.class).to(shuffler);
  }
  
}
