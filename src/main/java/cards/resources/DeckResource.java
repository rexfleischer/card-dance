package cards.resources;

import cards.data.Deck;
import cards.repository.DeckNotFoundException;
import cards.repository.DeckRepository;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.inject.Inject;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * an important design feature is always separation of concerns. 
 * in this context it means that the resources will never contain 
 * business logic. the only jobs for resources are to convert the
 * http request into a request for a repository, then convert the 
 * response from the repository into an http response.
 */
@Path("decks")
public class DeckResource {
  
  private final DeckRepository decks;
  private final Gson gson;

  @Inject
  public DeckResource(DeckRepository decks, Gson gson) {
    this.decks = decks;
    this.gson = gson;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response list() {
    String json = gson.toJson(decks.getDeckNames());
    return Response.ok().entity(json).build();
  }

  @GET
  @Path("{name}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response get(@PathParam("name") String name) {
    try {
      Deck deck = decks.getDeck(name);
      String json = gson.toJson(deck);
      return Response.ok().entity(json).build();
    }
    catch(DeckNotFoundException ex) {
      return deckNotFound(name);
    }
  }
  
  @PUT
  @Path("{name}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response ensure(@PathParam("name") String name) {
    Deck deck = decks.ensureDeck(name);
    String json = gson.toJson(deck);
    return Response.ok().entity(json).build();
  }
  
  @DELETE
  @Path("{name}")
  public Response delete(@PathParam("name") String name) {
    try {
      decks.deleteDeck(name);
      return Response.ok().build();
    }
    catch(DeckNotFoundException ex) {
      return deckNotFound(name);
    }
  }

  @POST
  @Path("{name}/shuffle")
  @Produces(MediaType.APPLICATION_JSON)
  public Response shuffle(@PathParam("name") String name) {
    try {
      Deck deck = decks.shuffleDeck(name);
      String json = gson.toJson(deck);
      return Response.ok().entity(json).build();
    }
    catch(DeckNotFoundException ex) {
      return deckNotFound(name);
    }
  }
  
  private Response deckNotFound(String name) {
    String message = gson.toJson(ImmutableMap.of(
        "error", "deck not found",
        "deck", name));
    return Response
        .status(Response.Status.NOT_FOUND)
        .entity(message)
        .build();
    
  }
  
}
