package cards.server;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.reflect.ClassPath;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import java.io.IOException;
import java.util.Set;

/**
 * manages a jetty server.
 * 
 * note how injection happens on the constructor. this is
 * a common pattern that i use to help with testing and 
 * reuse of the class. this means if we are testing just
 * this class (or some other class that does the same thing, like the repo)
 * we can mock out the dependencies if someone wants. i generally
 * don't like mocking, but sometimes it helps with the speed of
 * the test.
 */
@Singleton
public class ServerManager {

  private static final Logger logger = LogManager.getLogger();
  
  private final int port;
  private final Injector injector;
  private Server server;

  @Inject
  public ServerManager(Injector injector, @ServerPort int port) {
    this.injector = injector;
    this.port = port;
  }

  /**
   * starts the jetty server on the port specified in the constructor
   */
  public synchronized void start() {
    logger.info("starting server: port -> {}", port);
    Preconditions.checkState(server == null);

    ResourceConfig config = new ResourceConfig();
    for(ClassPath.ClassInfo info : paths()) {
      Class<?> clazz = info.load();
      logger.info("setting up resource: {}", clazz);
      Object resource = injector.getInstance(clazz);
      injector.injectMembers(resource);
      config.register(resource);
    }
    
    server = new Server(port);
    ServletContextHandler context = new ServletContextHandler(server, "/*");
    context.addServlet(new ServletHolder(new ServletContainer(config)), "/*");
    
    try {
      server.start();
    }
    catch(Exception ex) {
      logger.error("error while starting server", ex);
      server.destroy();
      throw Throwables.propagate(ex);
    }
  }
  
  public synchronized void stop() {
    logger.info("starting server: port -> {}", port);
    if (server == null) {
      return;
    }

    try {
      server.stop();
    }
    catch(Exception ex) {
      // we dont want to throw an exception because there will 
      // be no way to handle it. so just logging it to make sure
      // we do the best cleanup we can is good enough.
      logger.error("error while stopping server", ex);
    }
    server.destroy();
    server = null;
  }
  
  public synchronized void join() {
    logger.info("joining server: port -> {}", port);
    Preconditions.checkState(server != null);
    try {
      server.join();
    }
    catch(InterruptedException ex) {
      throw Throwables.propagate(ex);
    }
  }
  
  private Set<ClassPath.ClassInfo> paths() {
    try {
      return ClassPath
          .from(getClass().getClassLoader())
          .getTopLevelClasses("cards.resources");
    }
    catch(IOException ex) {
      logger.error("error while getting resource classes", ex);
      throw Throwables.propagate(ex);
    }
  }
  
}
