package cards.data;

import cards.data.impl.DeckDAOImpl;
import cards.inject.PersistenceModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RunWith(Parameterized.class)
public class DeckDAOTest {
  
  @Parameterized.Parameters
  public static List<Object[]> data() {
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[]{ DeckDAOImpl.class });
    // as more daos get added, make sure to add the implementation here
    // and that they pass for at least minimum functionality...
    // though, right now theres not even a way to specify an implementation,
    // making that happen would take just changing PersistenceModule
    return result;
  }
  
  private final Class<? extends DeckDAO> daoClazz;
  private Injector injector;
  private DeckDAO decks;

  public DeckDAOTest(Class<? extends DeckDAO> daoClazz) {
    this.daoClazz = daoClazz;
  }
  
  @Before
  public void before() {
    // make sure to add cleaning if the dao is persistent
    injector = Guice.createInjector(new PersistenceModule());
    decks = injector.getInstance(DeckDAO.class);
  }
  
  @Test
  public void ensureDaoTypeIsCorrect() {
    Assert.assertEquals(daoClazz, decks.getClass());
  }
  
  @Test
  public void testCounts() {
    Assert.assertTrue(decks.getDeckNames().isEmpty());
    
    decks.saveDeck(new Deck("test1"));
    decks.saveDeck(new Deck("test2"));
    decks.saveDeck(new Deck("test3"));
    
    Assert.assertEquals(3, decks.getDeckNames().size());
  }
  
  @Test
  public void testGets() {
    decks.saveDeck(new Deck("test1"));
    decks.saveDeck(new Deck("test2"));
    decks.saveDeck(new Deck("test3"));
    
    Assert.assertNotNull(decks.getDeck("test1"));
    Assert.assertEquals("test1", decks.getDeck("test1").name);

    Assert.assertNotNull(decks.getDeck("test2"));
    Assert.assertEquals("test2", decks.getDeck("test2").name);

    Assert.assertNotNull(decks.getDeck("test3"));
    Assert.assertEquals("test3", decks.getDeck("test3").name);
  }
  
  @Test
  public void testDelete() {
    decks.saveDeck(new Deck("test1"));
    decks.saveDeck(new Deck("test2"));
    decks.saveDeck(new Deck("test3"));
    
    Assert.assertFalse(decks.deleteDeck("not-found"));
    Assert.assertTrue(decks.deleteDeck("test2"));
    
    Assert.assertNotNull(decks.getDeck("test1"));
    Assert.assertNull(decks.getDeck("test2"));
    Assert.assertNotNull(decks.getDeck("test3"));
  }
  
  @Test
  public void testGetNames() {
    decks.saveDeck(new Deck("test1"));
    decks.saveDeck(new Deck("test2"));
    decks.saveDeck(new Deck("test3"));
    
    Set<String> namesBefore = decks.getDeckNames();
    Assert.assertEquals(3, namesBefore.size());
    Assert.assertTrue(namesBefore.contains("test1"));
    Assert.assertTrue(namesBefore.contains("test2"));
    Assert.assertTrue(namesBefore.contains("test3"));

    Assert.assertTrue(decks.deleteDeck("test2"));
    
    Set<String> namesAfter = decks.getDeckNames();
    Assert.assertEquals(2, namesAfter.size());
    Assert.assertTrue(namesBefore.contains("test1"));
    Assert.assertTrue(namesBefore.contains("test3"));
  }
  
}
