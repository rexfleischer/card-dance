package cards.data;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class DeckTest {

  /**
   * we want to make sure that a new deck is the way
   * we intend it to be. its the starting point of a lot
   * of business logic, so we have to know we started correctly.
   */  
  @Test
  public void testNewDeck() {
    Deck deck = new Deck("test");
    
    Assert.assertEquals(52, deck.cards.size());
    
    Assert.assertEquals(4, deck.cards.stream().map(c -> c.suit).distinct().count());
    Assert.assertEquals(13, deck.cards.stream().map(c -> c.face).distinct().count());
    
    for(DeckCard.Face face : DeckCard.Face.values()) {
      // if we get all the cards of a certain face, it should always be 4 cards
      // and from all four suits.
      List<DeckCard> cards = deck.cards.stream()
          .filter(c -> c.face == DeckCard.Face.ACE)
          .collect(Collectors.toList());
      Assert.assertEquals(4, cards.size());
      Assert.assertEquals(4, cards.stream().map(c -> c.suit).distinct().count());
    }
  }
  
}
