package cards.data;

import cards.data.impl.ComplexDeckShuffler;
import cards.data.impl.SimpleDeckShuffler;
import cards.inject.ConfigModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;

@RunWith(Parameterized.class)
public class DeckShufflerTest {
  @Parameterized.Parameters
  public static List<Object[]> data() {
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[]{ ComplexDeckShuffler.class });
    result.add(new Object[]{ SimpleDeckShuffler.class });
    // if more shufflers are made, make sure to add them here
    return result;
  }
  
  
  private final Class<? extends DeckShuffler> shufflerClass;
  private Injector injector;
  private DeckShuffler shuffler;

  public DeckShufflerTest(Class<? extends DeckShuffler> shuffler) {
    this.shufflerClass = shuffler;
  }

  @Before
  public void before() {
    injector = Guice.createInjector(new ConfigModule(0, shufflerClass));
    shuffler = injector.getInstance(DeckShuffler.class);
  }

  @Test
  public void ensureShufflerTypeIsCorrect() {
    Assert.assertEquals(shufflerClass, shuffler.getClass());
  }
  
  @Test(timeout = 500) // should be at least a little fast
  public void testShuffleMinimallyWorks() {
    Deck deck = new Deck("test");
    Deck shuffled = shuffler.shuffle(deck);
    
    Assert.assertEquals(deck.name, shuffled.name);
    Assert.assertEquals(deck.cards.size(), shuffled.cards.size());
    Assert.assertNotEquals(deck, shuffled);
    Assert.assertNotEquals(deck.cards, shuffled.cards);
  }
  
}
