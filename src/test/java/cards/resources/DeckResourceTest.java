package cards.resources;

import cards.data.Deck;
import cards.data.impl.SimpleDeckShuffler;
import cards.inject.InjectorFactory;
import cards.server.ServerManager;
import com.google.gson.Gson;
import com.google.inject.Injector;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class DeckResourceTest {

  private static CloseableHttpClient client;
  private static ResponseHandler<String> responseHandler = response -> {
    int status = response.getStatusLine().getStatusCode();
    if (status >= 200 && status < 300) {
      HttpEntity entity = response.getEntity();
      return entity != null ? EntityUtils.toString(entity) : null;
    } else {
      throw new ClientProtocolException("Unexpected response status: " + status);
    }
  };
  
  private Injector injector;
  private Gson gson;
  
  @BeforeClass
  public static void beforeClass() throws Exception {
    client = HttpClients.createDefault();
  }
  
  @AfterClass
  public static void afterClass() throws Exception {
    if (client != null) {
      client.close();
      client = null;
    }
  }

  @Before
  public void setup() {
    injector = InjectorFactory.get(8090, SimpleDeckShuffler.class);
    injector.getInstance(ServerManager.class).start();
    gson = injector.getInstance(Gson.class);
  }
  
  @After
  public void after() {
    injector.getInstance(ServerManager.class).stop();
  }
  
  
  
  @Test
  public void testHappyPath() throws Exception {

    // make sure an empty list works
    HttpGet getList = new HttpGet("http://localhost:8090/decks");
    try(CloseableHttpResponse response = client.execute(getList)) {
      Assert.assertEquals(200, response.getStatusLine().getStatusCode());
      String body = EntityUtils.toString(response.getEntity());
      String[] names = gson.fromJson(body, String[].class);
      Assert.assertEquals(0, names.length);
    }
    
    // create a few decks
    for(int i = 0; i < 5; i++) {
      HttpPut ensureDeck = new HttpPut("http://localhost:8090/decks/test" + i);
      try(CloseableHttpResponse response = client.execute(ensureDeck)) {
        Assert.assertEquals(200, response.getStatusLine().getStatusCode());
        String body = EntityUtils.toString(response.getEntity());
        Deck deck = gson.fromJson(body, Deck.class);
        Assert.assertEquals("test" + i, deck.name);
      }
    }

    // get a list of those decks
    try(CloseableHttpResponse response = client.execute(getList)) {
      Assert.assertEquals(200, response.getStatusLine().getStatusCode());
      String body = EntityUtils.toString(response.getEntity());
      Set<String> names = new LinkedHashSet<>(Arrays.asList(
          gson.fromJson(body, String[].class)));
      Assert.assertEquals(5, names.size());

      Assert.assertTrue(names.contains("test0"));
      Assert.assertTrue(names.contains("test1"));
      Assert.assertTrue(names.contains("test2"));
      Assert.assertTrue(names.contains("test3"));
      Assert.assertTrue(names.contains("test4"));
    }
    
    // get a single deck
    HttpGet getSingle = new HttpGet("http://localhost:8090/decks/test1");
    try(CloseableHttpResponse response = client.execute(getSingle)) {
      Assert.assertEquals(200, response.getStatusLine().getStatusCode());
      String body = EntityUtils.toString(response.getEntity());
      Deck deck = gson.fromJson(body, Deck.class);
      Assert.assertEquals("test1", deck.name);
    }

    // delete that deck
    HttpDelete delete = new HttpDelete("http://localhost:8090/decks/test1");
    try(CloseableHttpResponse response = client.execute(delete)) {
      Assert.assertEquals(200, response.getStatusLine().getStatusCode());
    }

    // try to get again.. should be 404
    try(CloseableHttpResponse response = client.execute(getSingle)) {
      Assert.assertEquals(404, response.getStatusLine().getStatusCode());
      String body = EntityUtils.toString(response.getEntity());
      Map error = gson.fromJson(body, Map.class);
      Assert.assertEquals(2, error.size());
      Assert.assertEquals("deck not found", error.get("error"));
      Assert.assertEquals("test1", error.get("deck"));
    }
    
    // test the shuffled endpoint
    Deck test2 = requestForDeck(new HttpGet("http://localhost:8090/decks/test2"));
    Deck shuffled = requestForDeck(new HttpPost("http://localhost:8090/decks/test2/shuffle"));
    Assert.assertEquals(test2.name, shuffled.name);
    Assert.assertNotEquals(test2.cards, shuffled.cards);
  }
  
  
  private Deck requestForDeck(HttpUriRequest request) throws Exception {
    try(CloseableHttpResponse response = client.execute(request)) {
      Assert.assertEquals(200, response.getStatusLine().getStatusCode());
      String body = EntityUtils.toString(response.getEntity());
      return gson.fromJson(body, Deck.class);
    }
  }
  
}
