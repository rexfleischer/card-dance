package cards.repository;

import cards.data.Deck;
import cards.data.impl.SimpleDeckShuffler;
import cards.inject.InjectorFactory;
import com.google.inject.Injector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DeckRepositoryTest {

  /**
   * some may not like using an injector in unit tests and want
   * to use mocking for unit tests.
   * 
   * while i'm ok with doing that, i disagree with it because
   * mocking looks a lot like injection anyways. except the test
   * has to do the setup... so what tests the tests setup?
   * 
   * i think testing the system as close to its working state as
   * much as possible is the correct way to go. granted, if the test
   * takes a long time to setup or clean, or is just not practical 
   * in a sense of resources or some other restraint; then yea,
   * mocking would be awesome.
   */
  private Injector injector;
  private DeckRepository repository;
  
  @Before
  public void setup() {
    injector = InjectorFactory.get(0, SimpleDeckShuffler.class);
    repository = injector.getInstance(DeckRepository.class);
  }
  
  @Test(expected = DeckNotFoundException.class)
  public void testNotFound() throws Exception {
    repository.getDeck("notfound");
  }
  
  @Test
  public void testSimpleGet() throws Exception {
    repository.ensureDeck("test1");
    Deck test1 = repository.getDeck("test1");
    Assert.assertNotNull(test1);
    Assert.assertEquals("test1", test1.name);
  }
  
  @Test
  public void testEnsureIsIdempotent() throws Exception {
    Deck test1 = repository.ensureDeck("test1");
    for(int i = 0; i < 1000; i++) {
      Deck check = repository.ensureDeck("test1");
      Assert.assertEquals(test1, check);
      Assert.assertTrue(test1 == check);
    }
  }
  
  @Test
  public void testShuffleDeck() throws Exception {
    Deck test = repository.ensureDeck("test");
    Deck shuffled = repository.shuffleDeck("test");
    
    // because shuffling is already tested pretyt well, we just check to make sure
    // that it actually happened, but not the quality of the shuffle
    Assert.assertEquals(test.name, shuffled.name);
    Assert.assertEquals(test.cards.size(), shuffled.cards.size());
    Assert.assertNotEquals(test, shuffled);
    
    Deck shuffledCheck = repository.getDeck("test");
    Assert.assertEquals(shuffled, shuffledCheck);
  }
  
}
